package main;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;
import java.util.Map;

public class BrowserFactory {

    private static WebDriver driver;
    private static ChromeOptions chromeOptions;

    public static void setDriver(){
        System.setProperty("webdriver.chrome.driver","C:\\lib\\chromedriver.exe");
        setBrowserCapabilities();
        driver = new ChromeDriver(chromeOptions);
    }

    public static void tearDownDriver(){
        if (driver !=null) {
            driver.close();
            driver.quit();
        }
    }

    public static WebDriver getDriver(){
        return driver;
    }

    private static void setBrowserCapabilities() {

       String downloadDir = (new StringBuilder())
                .append(Main.workspacePath)
                .append("result")
                .append(System.getProperty("file.separator"))
                .append("attachments")
                .append(System.getProperty("file.separator"))
                .toString();

        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_settings.popups",0);
        prefs.put("download.default_directory", downloadDir);

        chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("prefs", prefs);
        //chromeOptions.addArguments("--start-fullscreen");
    }


}