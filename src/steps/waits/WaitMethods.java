package steps.waits;

import main.BrowserFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static extent.reports.ExtentManager.logInfo;

public class WaitMethods {

    private static WebDriver jsWaitDriver = BrowserFactory.getDriver();
    private static WebDriverWait jsWait = new WebDriverWait(jsWaitDriver, 10);
    private static JavascriptExecutor jsExec = (JavascriptExecutor) jsWaitDriver;;

    private static WebDriverWait getWebDriverWait(int seconds){
        return new WebDriverWait(BrowserFactory.getDriver(),seconds);
    }

    public static void waitUntilElementIsVisible(WebElement element, int seconds){
        getWebDriverWait(seconds).until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitUntilElementIsClickable(WebElement element, int seconds){
        getWebDriverWait(seconds).until(ExpectedConditions.elementToBeClickable(element));
    }

    public static boolean waitUntilElementIsNotPresent(WebElement element, long loopingTime) {
        long end = System.currentTimeMillis() + (loopingTime * 1000);

        while(System.currentTimeMillis() < end) {
            try {
                element.isDisplayed();
            } catch (NoSuchElementException ex) {
                return true;
            }
        }
        return false;
    }

    public static int waitUntilOneElementIsDisplayed(WebElement element1, WebElement element2, long loopingTime) {
        long end = System.currentTimeMillis() + (loopingTime * 1000);

        while(System.currentTimeMillis() < end) {
            try {
                if (element1.getText() != null) {
                    return 1;
                }
            } catch (NoSuchElementException ex){
                //just wait
            }

            try {
                if (element2.getText() != null) {
                    return 2;
                }
            } catch (NoSuchElementException ex){
                //just wait
            }
        }
        return 0;
    }

    public static void waitForJQueryLoad() {
        try {
            ExpectedCondition<Boolean> jQueryLoad = driver -> ((Long) ((JavascriptExecutor) jsWaitDriver)
                    .executeScript("return jQuery.active") == 0);

            boolean jqueryReady = (Boolean) jsExec.executeScript("return jQuery.active==0");

            if (!jqueryReady) {
                jsWait.until(jQueryLoad);
            }
        } catch (WebDriverException ignored) {
        }
    }

}
